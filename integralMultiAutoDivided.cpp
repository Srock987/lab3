#include <iostream>
#include <array>
#include <omp.h>

double function(double x) {
    return 1 / (1 + x * x);
}

double integral(double start, double end, unsigned long long steps, double (*fun)(double)) {
    double result{0};
    double stepWidth{(end - start) / steps};
    for (unsigned i=0; i < steps; i++)
        result += stepWidth * fun(start + i * stepWidth);

    return result;
}

int main (int argc, char** argv) {
    const auto steps = 1000000000;
    double start{0};
    double end{1};
    double sum{0};
    double stepWidth{(end - start) / steps};

#pragma omp parallel for reduction(+: sum)
    for (unsigned int i = 0; i < steps; i++)
        sum += stepWidth * function(start + i * stepWidth);

    std::cout << 4 * sum << std::endl;
}
