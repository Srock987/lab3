#include <iostream>
#include <omp.h>
#include <random>
#include <chrono>

int main (int argc, char** argv) {
    unsigned pointsNumber{10000000};
    unsigned pointsInCircle = 0;
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(0, 1.0);
    auto start = std::chrono::steady_clock::now();

#pragma omp parallel for reduction(+: pointsInCircle)
    for (unsigned i = 0; i < pointsNumber; ++i)
    {
        double x = dist(mt);
        double y = dist(mt);

        if (x * x + y * y <= 1)
            ++pointsInCircle;
    }

    std::cout << 4 * ((double)pointsInCircle/pointsNumber) << std::endl;
    auto end = std::chrono::steady_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << std::endl;
    return 0;
}