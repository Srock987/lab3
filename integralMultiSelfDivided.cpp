#include <iostream>
#include <array>
#include <omp.h>

double function(double x) {
    return 1 / (1 + x * x);
}

double integral(double start, double end, unsigned long long steps, double (*fun)(double)) {
    double result{0};
    double stepWidth{(end - start) / steps};
    for (unsigned i = 0; i < steps; i++)
        result += stepWidth * fun(start + i * stepWidth);

    return result;
}

int main(int argc, char **argv) {

    int threadId;
    std::array<double, 4> results{0};
    const auto steps = 1000000000;

#pragma omp parallel private(threadId)
    {
        threadId = omp_get_thread_num();
        results[threadId] = integral(0 + threadId * 0.25, 0.25 + threadId * 0.25, steps / 4, function);
    }

    double result{0};
    for (auto val : results)
        result += val;

    std::cout << 4 * result << std::endl;
    return 0;
}
