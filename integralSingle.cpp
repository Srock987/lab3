#include <iostream>

double function(double x) {
    return 1 / (1 + x*x);
}

double integral(double start, double end, unsigned long long steps, double (*fun)(double)) {
    double result{0};
    double stepWidth{(end - start) / steps};
    for (unsigned i=0; i < steps; i++)
        result += stepWidth * fun(start + i * stepWidth);
    return result;
}

int main (int argc, char** argv) {
    const auto steps = 1000000000;
    std::cout << 4 * integral(0, 1, steps, function) << std::endl;
    return 0;
}